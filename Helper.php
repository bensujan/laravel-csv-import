<?php

namespace App\Helpers;

use DB;

class Helper
{

    /**
     * @param string $table table name
     * @param string $dumpname .csv file name without extension
     * @param string $path file path inside storage directory
     * @param array $fields
     * @return boolean
     *
     * laravel database seed helper function for seeding table with data from csv
     *
     * Example:
     * Helper::seedCsv('users','users','path to csv file in storage directory [storage/csv by default]',array('id','username','password','email'))
     *
     * $fields parameter indicated the columns to import from csv. Imports all columns if empty
     * If column names in .csv file and database table is different, .csv file column can be mapped to table column name as array('csvColumn'=>'tableColumn')
     * Example:
     * Table Columns: id, username, password, email
     * CSV Columns: id, uname, pass, email
     * $fields parameter vallue passed as : array('id','uname'=>'username','pass'=>'password','email)
     *
     */
    public static function seedCsv($table = '', $dumpname = '', $path = '', array $fields)
    {
        ini_set('auto_detect_line_endings', true);
        $csvPath = !empty($path) ? $path : 'csv';
        $fileName = $csvPath . '/' . $dumpname . '.csv';
        $filePath = storage_path($fileName);
        $file = fopen($filePath, "r");
        $insertArr = array();
        $i = 0;
        $columns = [];
        while (($data = fgetcsv($file, 200, ",")) !== FALSE) {
            if ($i > 0) {
                $row = [];
                foreach ($data as $k => $val) {
                    if(!empty($fields)) {
                        foreach ($fields as $fk=>$field) {
                            $ifk = is_numeric($fk)?$field:$fk;
                            if ($columns[$k] == $ifk) {
                                $row[$field] = $val;
                            }
                        }
                    }
                    else{
                        $row[$columns[$k]] = $val;
                    }
                }
                array_push($insertArr, $row);
            } else {
                $columns = $data;
            }
            $i++;
        }
        fclose($file);
        return DB::table($table)->insert($insertArr);
    }

}