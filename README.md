# README #

Helper function to import csv file to database tables in laravel.

### What is this repository for? ###

* Imports .csv file from storage directory of laravel to database
* 1.0

### How do I set up? ###

* Add csv file to storage/csv directory and addd this helper class inside app/Helpers directory
* Configuration
* Uses Laravel DB
* Include Helper class and call seedCsv method with appropirate parameters.
* laravel database seed helper function for seeding table with data from csv

### Example ###
* Helper::seedCsv('users','users','path to csv file in storage directory [storage/csv by default]',array('id','username','password','email'))

* $fields parameter indicated the columns to import from csv. Imports all columns if empty
* If column names in .csv file and database table is different, .csv file column can be mapped to table column name as array('csvColumn'=>'tableColumn')
* Example:
* Table Columns: id, username, password, email
* CSV Columns: id, uname, pass, email
* $fields parameter vallue passed as : array('id','uname'=>'username','pass'=>'password','email)

### Who do I talk to? ###

* [Sujan Byanjankar](https://bitbucket.org/bensujan)